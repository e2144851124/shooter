// Fill out your copyright notice in the Description page of Project Settings.


#include "Ammo.h"
#include "Particles/ParticleSystemComponent.h"
#include "GameFramework/Actor.h"
#include "Components/TextRenderComponent.h"
#include "Components/StaticMeshComponent.h"

// Sets default values
AAmmo::AAmmo()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Root = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	SetRootComponent(Root);

	AmmoCapsuleComponent = CreateDefaultSubobject<UCapsuleComponent>(TEXT("CapsuleComponent"));
	AmmoCapsuleComponent->SetupAttachment(Root);

	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	Mesh->SetupAttachment(Root);

	AmmoTextRenderComponent = CreateDefaultSubobject<UTextRenderComponent>(TEXT("AmmoText"));
	AmmoTextRenderComponent->SetupAttachment(Root);
	AmmoTextRenderComponent->SetTextRenderColor(FColor::White);
}

// Called when the game starts or when spawned
void AAmmo::BeginPlay()
{
	Super::BeginPlay();
	if (AmmoParticleCircleClass)
	{
		AmmoParticleCircle = GetWorld()->SpawnActor<AActor>(AmmoParticleCircleClass,this->GetActorLocation(),this->GetActorRotation());
		// AmmoParticleCircle->SetOwner(this);
		
	}

	// FText AmmoText;
	// AmmoText.AsCultureInvariant(TEXT("Press E to pick"));
	AmmoTextRenderComponent->SetText(TEXT("Press E to pick"));
	
}

// Called every frame
void AAmmo::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

