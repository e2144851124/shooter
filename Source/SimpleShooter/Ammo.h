// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Particles/ParticleSystemComponent.h"
#include "Components/TextRenderComponent.h"
#include "Components/CapsuleComponent.h"
#include "Ammo.generated.h"

UCLASS()
class SIMPLESHOOTER_API AAmmo : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AAmmo();

	int GetAmmoCount();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(BlueprintReadWrite,EditAnywhere)
	AActor* AmmoParticleCircle;
private:	

	UPROPERTY(EditAnywhere)
	USceneComponent* Root;	

	UPROPERTY(EditAnywhere);
	UStaticMeshComponent* Mesh;

	UPROPERTY(EditAnywhere)
	UCapsuleComponent* AmmoCapsuleComponent;	

	UPROPERTY(EditAnywhere)
	int AmmoCount;

	UPROPERTY(EditAnywhere)
	TSubclassOf<AActor> AmmoParticleCircleClass;

	UPROPERTY(EditAnywhere)
	UTextRenderComponent* AmmoTextRenderComponent;
};
