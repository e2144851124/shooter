// Fill out your copyright notice in the Description page of Project Settings.

#include "Gun.h"
#include "Components/SkeletalMeshComponent.h"
#include "Kismet/GameplayStatics.h"
#include "DrawDebugHelpers.h"
#include "Engine/World.h"

// Sets default values
AGun::AGun()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Root = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	SetRootComponent(Root);

	Mesh = CreateAbstractDefaultSubobject<USkeletalMeshComponent>(TEXT("Mesh"));
	Mesh->SetupAttachment(Root);

	ProjectileSpawnPoint = CreateDefaultSubobject<USceneComponent>(TEXT("Projectile Spawn Point"));
	ProjectileSpawnPoint->SetupAttachment(Mesh);
}

// Called when the game starts or when spawned
void AGun::BeginPlay()
{
	Super::BeginPlay();
}


// Called every frame
void AGun::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AGun::PullTrigger()
{
	UGameplayStatics::SpawnEmitterAttached(MuzzleFlash, Mesh , TEXT("MuzzleFlashSocket"));
	UGameplayStatics::SpawnSoundAttached(MuzzleSound, Mesh , TEXT("MuzzleFlashSocket"));

		
	FHitResult  Hit;
	FVector ShotDirection;


	bool bIsHit = GunTrace(Hit,ShotDirection);

	if (bIsHit)
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(),ImpactEffect,Hit.Location,ShotDirection.Rotation());
		UGameplayStatics::PlaySoundAtLocation(GetWorld(),ImpactSound,Hit.Location,ShotDirection.Rotation());
		AActor* HitActor = Hit.GetActor();
		UE_LOG(LogTemp,Warning,TEXT("Hit Actor:	%s"),*Hit.GetActor()->GetName());
		if (HitActor != NULL)
		{
			FPointDamageEvent DamageEvent(Damage,Hit,ShotDirection,nullptr);
			AController* OwnerController = GetOwnerController();
			if (OwnerController == nullptr) return;
			HitActor->TakeDamage(Damage, DamageEvent, OwnerController, this);
		}
	}
}

void AGun::SpawnProjectile() 
{
	if (ProjectileClass)
	{
		AController* OwnerController = GetOwnerController();
		if (OwnerController == nullptr)
		{
			return;
		}	
	
		FVector ViewLocation;
		FRotator ViewRotator;
		OwnerController->GetPlayerViewPoint(ViewLocation,ViewRotator);
		FVector SpawnLocation = ProjectileSpawnPoint->GetComponentLocation();
		FRotator SpawnRotation = ViewRotator;

		AProjectile* TempProjectile = GetWorld()->SpawnActor<AProjectile>(ProjectileClass,SpawnLocation,SpawnRotation);
		TempProjectile->SetOwner(this);
	}
}

bool AGun::GunTrace(FHitResult& Hit , FVector& ShotDirection) 
{
	AController* OwnerController = GetOwnerController();
	if (OwnerController == nullptr)
	{
		return false;
	}
	
	FVector ViewLocation;
	FRotator ViewRotator;
	OwnerController->GetPlayerViewPoint(ViewLocation,ViewRotator);
	ShotDirection = -ViewRotator.Vector();
	FVector End = ViewLocation + ViewRotator.Vector() * MaxRange;

	FCollisionQueryParams Params;
	Params.AddIgnoredActor(this);
	Params.AddIgnoredActor(GetOwner());
	return GetWorld()->LineTraceSingleByChannel(Hit,ViewLocation,End, ECC_GameTraceChannel1 ,Params);
}

AController* AGun::GetOwnerController() const
{
	APawn* OwnerPawn = Cast<APawn>(GetOwner());
	if (OwnerPawn == nullptr)	return nullptr;
	return OwnerPawn->GetController();
}