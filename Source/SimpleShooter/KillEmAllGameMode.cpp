// Fill out your copyright notice in the Description page of Project Settings.


#include "KillEmAllGameMode.h"
#include "ShooterPlayerController.h"
#include "EngineUtils.h"
#include "AIController.h"
#include "ShooterAIController.h"

void AKillEmAllGameMode::PawnKilled(APawn* PawnKilled) 
{
    Super::PawnKilled(PawnKilled);
    UE_LOG(LogTemp, Warning, TEXT("Pawn Killed !"));    

    APlayerController* PlayerController = Cast<AShooterPlayerController>(PawnKilled->GetController());
    if (PlayerController != nullptr)
    {
        EndGame(false);
        return;
    }

    // For loop over ShooterAI in world
    for (AShooterAIController* AIController : TActorRange<AShooterAIController>(GetWorld()))
    {
        // Is not Dead?
        if (!AIController->IsDead())
        {
            return;
        }
    }
    // EndGame
    EndGame(true);
}

void AKillEmAllGameMode::EndGame(bool bIsPlayerWinner) 
{
    for(AController* Controller : TActorRange<AController>(GetWorld()))
    {
        bool bIsWinner = bIsPlayerWinner == Controller->IsPlayerController();
        Controller->GameHasEnded(Controller->GetPawn(),bIsWinner);
    }
}