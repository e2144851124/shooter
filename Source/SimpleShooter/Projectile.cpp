// Fill out your copyright notice in the Description page of Project Settings.


#include "Projectile.h"
#include "Components/StaticMeshComponent.h"
#include "Particles/ParticleSystemComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
AProjectile::AProjectile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	// Set ProjectileMesh
	ProjectileMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Projectile Mesh"));
	ProjectileMesh->OnComponentHit.AddDynamic(this,&AProjectile::OnHit);
	RootComponent = ProjectileMesh;
 	// Set ProjectileMove ,initialize the value
	ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("Projectile Movement"));
	ProjectileMovement->InitialSpeed = MovementSpeed;
	ProjectileMovement->MaxSpeed = MovementSpeed;
	InitialLifeSpan = 3.f;
 	// Set Projectile Particle
	ParticleTrail = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("Particle Trail"));
	ParticleTrail->SetupAttachment(RootComponent);

}

// Called when the game starts or when spawned
void AProjectile::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AProjectile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AProjectile::OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	//Get reference to owning class.
	AActor* MyOwner = GetOwner();

	//Check MyOwner is not nullptr,if yes ,return as we should check the owner; 
	if (MyOwner == nullptr)
	{
		return;
	}

	//Check the actor is not itself OR Owner OR null , then apply damage 
	if (OtherActor && OtherActor != GetOwner() && OtherActor != this)
	{
		// IgnoreActors.Add(UGameplayStatics::GetPlayerController(GetWorld(),0)->GetPawn());

		UGameplayStatics::ApplyRadialDamageWithFalloff(GetWorld(),BaseDamage,MinDamage,GetActorLocation(),MinRadius,MaxRadius,DamageFallof,DamageType,IgnoreActors);
		
		UGameplayStatics::SpawnEmitterAtLocation(this, HitParticle, GetActorLocation(), GetActorRotation());
		UGameplayStatics::PlaySoundAtLocation(this, HitSound, GetActorLocation());

		Destroy();
	}
}

