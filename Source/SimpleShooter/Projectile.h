// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Particles/ParticleSystem.h"
#include "Particles/ParticleSystemComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Projectile.generated.h"

class UProjectileMovementComponent;

UCLASS()
class SIMPLESHOOTER_API AProjectile : public AActor
{
	GENERATED_BODY()
	
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Sets default values for this actor's properties
	AProjectile();

protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite);
	TArray<AActor*> IgnoreActors;

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	//COMPONENTS
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, category = "Components", meta = (AllowPrivateAccess = "true"));
	UProjectileMovementComponent* ProjectileMovement;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, category = "Components", meta = (AllowPrivateAccess = "true"));
	UStaticMeshComponent* ProjectileMesh;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, category = "Components", meta = (AllowPrivateAccess = "true"));
	UParticleSystemComponent* ParticleTrail;

	//VARIABLES
	UPROPERTY(EditDefaultsOnly, category = "Damage");
	TSubclassOf<UDamageType> DamageType;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, category = "Move", meta = (AllowPrivateAccess = "true"));
	float MovementSpeed = 1300.f;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, category = "Damage", meta = (AllowPrivateAccess = "true"));
	float BaseDamage = 75.f;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, category = "Damage", meta = (AllowPrivateAccess = "true"));
	float MinDamage = 50.f;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, category = "Damage", meta = (AllowPrivateAccess = "true"));
	float MaxRadius = 300.f;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, category = "Damage", meta = (AllowPrivateAccess = "true"));
	float MinRadius = 200.f;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, category = "Damage", meta = (AllowPrivateAccess = "true"));
	float DamageFallof = 0.2f;
	UPROPERTY(EditAnywhere, category = "Effects");
	UParticleSystem* HitParticle;
	UPROPERTY(EditAnywhere, category = "Effects");
	USoundBase* HitSound;

	UPROPERTY(EditAnywhere, category = "Effects");
	USoundBase* LaunchSound;
	UPROPERTY(EditAnywhere, category = "Effects");
	TSubclassOf<UCameraShake> HitShake;

	
	//FUNCTIONS
	UFUNCTION()
	void OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor,  UPrimitiveComponent* OtherComp,  FVector NormalImpulse, const FHitResult& Hit);

};