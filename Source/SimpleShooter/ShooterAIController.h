// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "ShooterAIController.generated.h"

/**
 * 
 */
UCLASS()
class SIMPLESHOOTER_API AShooterAIController : public AAIController
{
	GENERATED_BODY()
	
protected:
	void BeginPlay() override;
public:
	void Tick(float DeltaTime) override;

	bool IsDead() const;
private:
	UPROPERTY(EditAnywhere)
		class UBehaviorTree* AIBehavior;

	UPROPERTY(EditAnywhere)
	float RifleShootTime = 1.0f;
	
	UPROPERTY(EditAnywhere)
	float LauncherShootTime = 5.0f;	
};
