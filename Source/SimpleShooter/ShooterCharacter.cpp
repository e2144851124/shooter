// Fill out your copyright notice in the Description page of Project Settings.


#include "ShooterCharacter.h"
#include "Gun.h"
#include "Ammo.h"
#include "Components/CapsuleComponent.h"
#include "TimerManager.h"
#include "SimpleShooterGameModeBase.h"

// Sets default values
AShooterCharacter::AShooterCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	AmmoTextRenderComponent = CreateDefaultSubobject<UTextRenderComponent>(TEXT("AmmoText"));
	AmmoTextRenderComponent->SetupAttachment(GetMesh());
	AmmoTextRenderComponent->SetTextRenderColor(FColor::Yellow);
}

// Called when the game starts or when spawned
void AShooterCharacter::BeginPlay()
{
	Super::BeginPlay();
	
	Health = MaxHealth;

	SetAmmoText(TEXT("Rifle :"),RifleAmmo);
	
	SetupWeapon();
}


// Called every frame
void AShooterCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AShooterCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis(TEXT("MoveForward"), this, &AShooterCharacter::MoveForward);
	PlayerInputComponent->BindAxis(TEXT("LookUp"), this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis(TEXT("MoveRight"), this, &AShooterCharacter::MoveRight);
	PlayerInputComponent->BindAxis(TEXT("LookRight"), this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAction(TEXT("Jump"), IE_Pressed, this, &AShooterCharacter::Jump);
	PlayerInputComponent->BindAxis(TEXT("LookRightRate"), this,&AShooterCharacter::LookRightRate);
	PlayerInputComponent->BindAxis(TEXT("LookUpRate"), this, &AShooterCharacter::LookUpRate);
	PlayerInputComponent->BindAction(TEXT("Shoot"), IE_Pressed, this,&AShooterCharacter::Shoot);
	PlayerInputComponent->BindAction(TEXT("ChangeWeapon"), IE_Pressed, this, &AShooterCharacter::ChangeWeapon);

	

}

float AShooterCharacter::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	float DamageApplied = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
	DamageApplied = FMath::Min(Health, DamageApplied);
	Health -= DamageApplied;
	UE_LOG(LogTemp, Warning, TEXT("Health left : %f"), Health);

	if(IsDead())
	{
		if (bIsDrop)
		{
			SpawnAmmo();
			bIsDrop = false;
		}
		ASimpleShooterGameModeBase* GameMode = GetWorld()->GetAuthGameMode<ASimpleShooterGameModeBase>();
		if (GameMode != nullptr)
		{
			GameMode->PawnKilled(this);
			GetWorldTimerManager().SetTimer(DestroyTimerHandle, this,&AShooterCharacter::StartDestroy, DestroyDelay);
			
		}
		DetachFromControllerPendingDestroy();
		GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		
	}
	
	return DamageApplied;
}

bool AShooterCharacter::IsDead() const
{
	return Health <= 0;
}

float AShooterCharacter::GetHealthPercent() const
{
	return Health / MaxHealth;
}

void AShooterCharacter::MoveForward(float AxisValue)
{
	AddMovementInput(GetActorForwardVector() * AxisValue);
}

void AShooterCharacter::MoveRight(float AxisValue)
{
	AddMovementInput(GetActorRightVector() * AxisValue);
}

void AShooterCharacter::LookUpRate(float AxisValue)
{
	AddControllerPitchInput(AxisValue * GetWorld()->GetDeltaSeconds() * RotationRate);
}

void AShooterCharacter::LookRightRate(float AxisValue)
{
	AddControllerYawInput(AxisValue * GetWorld()->GetDeltaSeconds() * RotationRate);
}

void AShooterCharacter::Shoot()
{
	if(WeaponState == 1)
	{
		if (LauncherAmmo != 0)
		{
			Guns[WeaponState]->SpawnProjectile();
			--LauncherAmmo;
			SetAmmoText(TEXT("Launcher :"),LauncherAmmo);
			return;
		}
		return;
	}

	if (RifleAmmo != 0)
	{
		Guns[WeaponState]->PullTrigger();
		--RifleAmmo;
			SetAmmoText(TEXT("Rifle	:"),RifleAmmo);
		return;
	}
}

void AShooterCharacter::SetupWeapon() 
{
	GunClasses.Add(GunClassDefault);
	GunClasses.Add(GunClassAdd);
	// WeaponState = 0;
	AGun* Gun;

	for(auto& GunClass : GunClasses)
	{
		Gun = GetWorld()->SpawnActor<AGun>(GunClass);
		GetMesh()->HideBoneByName(TEXT("weapon_r"), EPhysBodyOp::PBO_None);
		Gun->AttachToComponent(GetMesh(), FAttachmentTransformRules::KeepRelativeTransform, TEXT("WeaponSocket"));
		Gun->SetOwner(this);
		Guns.Add(Gun);
		Gun->SetActorHiddenInGame(true);
	}
	Guns[WeaponState]->SetActorHiddenInGame(false);
}

void AShooterCharacter::SetAmmoText(FString Weapon,int AmmoCount) 
{
	// FText AmmoCountText;
	AmmoCountStr = Weapon + FString::FromInt(AmmoCount);
	// AmmoCountText.AsCultureInvariant(AmmoCountStr);
	AmmoTextRenderComponent->SetText(AmmoCountStr);
}

void AShooterCharacter::SpawnAmmo() 
{
	AAmmo* AmmoDrop = GetWorld()->SpawnActor<AAmmo>(AmmoClass,this->GetActorLocation() - FVector(0,0,90), this->GetActorRotation());
}

void AShooterCharacter::StartDestroy() 
{
	bool bIsDestroy = this->Destroy();
	bool bWeaponIsDestroy =	true;
	for(auto* AGun : Guns)
	{
		bWeaponIsDestroy = bWeaponIsDestroy == AGun->Destroy();
	}
	if (bIsDestroy && bWeaponIsDestroy)
	{
		UE_LOG(LogTemp,Error,TEXT("Destroy Successful !"));	
	}
}

void AShooterCharacter::ChangeWeapon() 
{
	if (WeaponState < WeaponMaxNum)
	{
		Guns[WeaponState]->SetActorHiddenInGame(true);
		Guns[++WeaponState]->SetActorHiddenInGame(false);
	}
	else
	{
		Guns[WeaponState]->SetActorHiddenInGame(true);
		WeaponState = 0;
		Guns[WeaponState]->SetActorHiddenInGame(false);
	}

	switch (WeaponState)
	{
	case 0:
		SetAmmoText(TEXT("Rifle	:"),RifleAmmo);
		break;
	case 1:
		SetAmmoText(TEXT("Launcher :"),LauncherAmmo);
		break;
	default:
		break;
	}
}

int AShooterCharacter::GetWeaponState() const
{
	return WeaponState;
}

	